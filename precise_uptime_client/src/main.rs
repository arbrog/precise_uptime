use chrono::Utc;
use dotenv::dotenv;
use log::{info, warn};
use precise_uptime_common::UptimePayload;
use rmp_serde::Serializer;
use serde::Serialize;
use std::env;
use std::{error::Error, time::Duration};
use tokio::{net::UdpSocket, time::sleep};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv().ok();
    env_logger::init();

    let id: u8 = match env::var("CLIENT_ID") {
        Ok(id) => id.parse::<u8>().unwrap(),
        Err(_) => {
            panic!("The 'CLIENT_ID' environment variable must be set and be a valid u8 number.")
        }
    };
    let server_ip = env::var("SERVER_IP").unwrap_or_else(|_| "127.0.0.1".to_string());
    let server_port = env::var("SERVER_PORT").unwrap_or_else(|_| "8080".to_string());

    let local_port = portpicker::pick_unused_port().expect("No ports free");
    let sock = UdpSocket::bind(format!("0.0.0.0:{local_port}")).await?;
    sock.connect(format!("{server_ip}:{server_port}")).await?;

    loop {
        let payload = UptimePayload {
            id: id,
            time_sent: Utc::now(),
        };
        let mut buf = Vec::new();
        payload
            .serialize(&mut Serializer::new(&mut buf).with_binary())
            .unwrap();
        match sock.send(&buf).await {
            Ok(_) => {
                info!("Message sent")
            }
            Err(e) => {
                warn!("Error sending message: {}; retrying.", e);
                continue;
            }
        };
        sleep(Duration::from_millis(500)).await;
    }
}
