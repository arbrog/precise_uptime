use chrono::{DateTime, Duration, Utc};
use dotenv::dotenv;
use influxdb::{InfluxDbWriteable, Query};
use log::{debug, error, info, warn};
use nohash_hasher::NoHashHasher;
use precise_uptime_common::{UptimePayload, REQUEST_FREQUENCY, REQUEST_GRACE_PERIOD};
use reqwest::{header, Client as RequestClient};
use rmp_serde::Deserializer;
use serde::Deserialize;
use std::{
    collections::HashMap, env, error::Error, hash::BuildHasherDefault, sync::Arc,
    time::Duration as StdDuration,
};
use tokio::{net::UdpSocket, sync::Mutex, time::sleep};

type ClientMap = Arc<Mutex<HashMap<u8, DateTime<Utc>, BuildHasherDefault<NoHashHasher<u8>>>>>;

#[derive(InfluxDbWriteable)]
struct ClientAlive {
    time: DateTime<Utc>,
    #[influxdb(field)]
    last_heard: i64,
    #[influxdb(field)]
    is_live: i8,
    #[influxdb(tag)]
    id: u8,
}

async fn check_liveliness(clients: ClientMap, request_client: RequestClient) {
    // env variables
    let influx_uri =
        env::var("INFLUX_URI").expect("The 'INFLUX_URI' environment variable must be set");
    let influx_bucket =
        env::var("INFLUX_BUCKET").expect("The 'INFLUX_BUCKET' environment variable must be set");
    let influx_org =
        env::var("INFLUX_ORG").expect("The 'INFLUX_ORG' environment variable must be set");
    let grade_period = REQUEST_GRACE_PERIOD.try_into().unwrap();

    loop {
        sleep(StdDuration::from_millis(REQUEST_FREQUENCY)).await;
        for (id, timestamp) in clients.lock().await.iter() {
            let current_time = Utc::now();
            let time_diff = current_time - *timestamp;
            let mut is_live = true;
            if time_diff > Duration::milliseconds(grade_period) {
                warn!("Internet outage detected at client {id}. Haven't heard from them in {time_diff}");
                is_live = false;
            }
            let is_live_number = match is_live {
                true => 1,
                false => -1,
            };
            let influx_event = ClientAlive {
                id: *id,
                last_heard: timestamp.timestamp(),
                is_live: is_live_number,
                time: Utc::now(),
            };

            info!("Pushing event for {id}, they are alive: {is_live}");
            let line_protocal_string = influx_event
                .into_query("uptime_ping")
                .build()
                .unwrap()
                .get();
            let write_result = request_client
                .post(format!("{influx_uri}/api/v2/write"))
                .query(&[
                    ("org", &influx_org),
                    ("bucket", &influx_bucket),
                    ("precision", &"ns".to_string()),
                ])
                .body(line_protocal_string)
                .send()
                .await;

            match write_result {
                Ok(res) => {
                    debug!("{}", res.status())
                }
                Err(error) => {
                    error!("Failed to push uptime ping to influx: {error}");
                }
            }
        }
    }
}

async fn accept_messages(socket: UdpSocket, clients: ClientMap) -> Result<(), Box<dyn Error>> {
    let mut buf = [0; 256];
    loop {
        let (len, addr) = socket.recv_from(&mut buf).await?;
        debug!("{:?} bytes received from {:?}", len, addr);

        let payload: UptimePayload =
            match Deserialize::deserialize(&mut Deserializer::new(buf.as_ref()).with_binary()) {
                Ok(payload) => payload,
                Err(e) => {
                    warn!("Failed to deserialize payload: {:?}", e);
                    continue;
                }
            };
        debug!("Recieved payload: {:?}", payload);
        clients.lock().await.insert(payload.id, payload.time_sent);
    }
}

fn setup_influx_client() -> RequestClient {
    let influx_token =
        env::var("INFLUX_TOKEN").expect("The 'INFLUX_TOKEN' environment variable must be set");

    //set headers
    let mut headers = header::HeaderMap::new();
    let mut influx_auth_token =
        header::HeaderValue::from_str(&format!("Bearer {influx_token}")).unwrap();
    influx_auth_token.set_sensitive(true);
    headers.insert(header::AUTHORIZATION, influx_auth_token);

    RequestClient::builder()
        .default_headers(headers)
        .gzip(true)
        .build()
        .unwrap()
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv().ok();
    env_logger::init();
    let request_client = setup_influx_client();

    let server_port = env::var("SERVER_PORT").unwrap_or_else(|_| "8080".to_string());
    let socket = UdpSocket::bind(format!("0.0.0.0:{server_port}")).await?;
    // TODO: would be more efficient to use message passing
    let clients: ClientMap = Arc::new(Mutex::new(HashMap::with_hasher(
        BuildHasherDefault::default(),
    )));

    tokio::select! {
        _ = accept_messages(socket, clients.clone()) => {},
        _ = check_liveliness(clients.clone(), request_client.clone()) => {},
    }

    Ok(())
}
