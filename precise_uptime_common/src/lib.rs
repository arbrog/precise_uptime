use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct UptimePayload {
    pub id: u8,
    pub time_sent: DateTime<Utc>,
}

//in milliseconds
pub const REQUEST_FREQUENCY: u64 = 500;
pub const REQUEST_GRACE_PERIOD: u64 = REQUEST_FREQUENCY * 2;
